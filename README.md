# Simple react web

Este proyecto es una aplicacion web hecha con react

## Getting Started

Seguir los pasos de abajo

### Prerequisites

La ultima version estable de node 

```
https://nodejs.org/es/
```

### Installing

Primero clonar el proyecto

```
git clone https://gitlab.com/roldy94/polymath-challenge
```

Despues instalar los node modolues

```
npm install
```

Por ultimo instalar json server

```
npm install -g json-server
```

## Running 

Para arrancar la aplicacion (recomiendo primero arrancar el servidor)

```
npm start
```

Para arrancar el server json 

```
cd api
```

```
json-server -p 8080 db.json
```

## Crendeciales para ingresar a la aplicacion

Usuarios
```
 admin
 pepe
 juan
```
Contraseña
```
 admin00
 pepe01
 juan01
```

## Versioning

Ninguna

## Authors

* **Nahuel Roldan** - *Initial work* - [Software Developer](https://gitlab.com/roldy94)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

