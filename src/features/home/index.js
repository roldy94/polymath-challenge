import React from "react";
import { connect } from "react-redux";
import * as LoginActions from '../login/actions';
import Sidebar from "./components/sidebar"
import AppRouter from "./components/app_router"
import { bindActionCreators } from "redux";
import { Actions } from "./actions";
import {
    LayoutHome,
    MenuHome,
    ContentHome,
    SiderHome,
    SubMenuHome,
    FooterHome,
    IconSpanHome,
    IconHome
} from './styles';

class Home extends React.Component {

    componentDidMount() {
        const { actions } = this.props;
        actions.setUrl(this.props.match)
        actions.homeRequest();
    }

    render() {
        const { url } = this.props.match;
        const { current } = this.props.state;
        const { actions,loginActions } = this.props;
        return (
            <LayoutHome>
                <SiderHome
                    collapsible
                    collapsed={this.props.state.collapsed}
                    onCollapse={() => actions.setCollapsed()}
                >
                    <Sidebar url={url} selectedKeys={current} onClickMenu={e => actions.changeOpenKeys([e.key])} />
                </SiderHome>
                <LayoutHome>
                    <MenuHome mode="horizontal">
                        <SubMenuHome onTitleClick={() => {
                            loginActions.signOut();
                            localStorage.clear();
                        }}
                            title={<IconSpanHome ><IconHome type="poweroff" />Log out</IconSpanHome>} />
                    </MenuHome>
                    <ContentHome>
                        <AppRouter url={url} />
                    </ContentHome>
                    <FooterHome>
                        Ant Design ©2020 Created by Nahuel Roldan
                    </FooterHome>

                </LayoutHome>
            </LayoutHome>
        )
    }
}

function mapStateToProps(state) {
    return {
        state: state.homeReducer,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ ...Actions }, dispatch),
        loginActions: bindActionCreators({ ...LoginActions.Actions }, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
