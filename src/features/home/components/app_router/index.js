import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import asyncComponent from '../../../../utils/asyncFunc';
import {
    Container,
} from './styles';

const routes = [
    {
        path: '',
        component: asyncComponent(() => import('../../../products/index')),
        
    },    
    {
        path: 'inicio/products',
        component: asyncComponent(() => import('../../../products/index')),
    },
];

class AppRouter extends Component {
    render() {
        const { url } = this.props;
        return (
            <Container>
                {routes.map(singleRoute => {
                    const { path, exact, ...otherProps } = singleRoute;
                    return (
                        <Route
                            exact={exact === false ? false : true}
                            key={singleRoute.path}
                            path={`${url}/${singleRoute.path}`}
                            {...otherProps}
                        />
                    );
                })}
            </Container>
        );
    }
}

export default AppRouter;
