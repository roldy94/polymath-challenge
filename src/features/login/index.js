import React from "react";
import {
    FormLogin,
    IconLogin, 
    InputLogin,
    ButtonLogin, 
    LayoutLogin,
    FormItemLogin,
    FooterLogin,
    ContentLogin,
    ContainerLogin,
    ContainerLogo,
    ContainerImg
} from './styles';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Actions } from "./actions";


class NormalLoginForm extends React.Component {
    
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    render() {

        const { getFieldDecorator } = this.props.form;
        const { actions } = this.props;
        return (
            <LayoutLogin>
                <ContentLogin>
                    <ContainerLogin>      
                        <ContainerLogo>
                            <ContainerImg src={require("../../assets/icon.png")} />
                        </ContainerLogo>                  
                        <FormLogin onSubmit={this.handleSubmit}>
                            <FormItemLogin>
                                {getFieldDecorator('userName', {
                                    rules: [{ required: true, message: 'Please enter your Username!' }],
                                })(
                                    <InputLogin prefix={<IconLogin type="user"/>} placeholder="Username"
                                        onChange={e => actions.setUsername(e.target.value)} />
                                )}
                            </FormItemLogin>
                            <FormItemLogin>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please enter your Password!' }],
                                })(
                                    <InputLogin prefix={<IconLogin type="lock"/>} type="password" placeholder="Password"
                                        onChange={e => actions.setPassword(e.target.value)}
                                        onKeyPress={e => e.key === "Enter" ?
                                            actions.loginRequest({ "username": this.props.state.username, "pass": this.props.state.password }) : null}
                                    />
                                )}
                            </FormItemLogin>
                            <FormItemLogin>

                                <ButtonLogin type="primary" className="login-form-button"
                                    onClick={() => actions.loginRequest({ "username": this.props.state.username, "pass": this.props.state.password })}
                                >
                                   Log in
                                </ButtonLogin>
                                
                            </FormItemLogin>
                        </FormLogin>
                    </ContainerLogin>
                </ContentLogin>
                <FooterLogin>
                    Ant Design ©2020 Created by Nahuel Roldan
                </FooterLogin>
                {this.props.state.isLoggedIn ? this.props.history.push("/inicio") : null}
            </LayoutLogin>

        );
    }
}

const WrappedNormalLoginForm = FormLogin.create()(NormalLoginForm);

function mapStateToProps(state) {
    return {
        state: state.loginReducer,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ ...Actions }, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm);