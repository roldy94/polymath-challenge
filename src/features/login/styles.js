import styled from 'styled-components';
import { Form, Icon, Input, Button, Layout } from 'antd';

const FormItem = Form.Item;
const { Footer, Content } = Layout;

export const FormLogin = styled(Form)`
    margin: auto;
    width: 25%;
    min-width: 250px;
    margin-top: 50px;
`;
export const IconLogin = styled(Icon)`
    color: rgba(0,0,0,.25);
`;
export const InputLogin = styled(Input)`
    color: rgba(0,0,0,.25)
`;
export const ButtonLogin = styled(Button)``;

export const LayoutLogin = styled(Layout)`
    background: white;
    height: 100%;
`;
export const FormItemLogin = styled(FormItem)``;

export const ContentLogin = styled(Content)`
    padding: 0 50px 
`;
export const FooterLogin = styled(Footer)`
    text-align: center;
`;
export const ContainerLogin = styled.div`
    padding: 4%;
`;

export const ContainerLogo = styled.div`
    text-align: center;
`;
export const ContainerImg = styled.img``;