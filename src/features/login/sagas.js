import { call, put, takeEvery } from "redux-saga/effects";
import { ActionsTypes, Actions } from "./actions";

import { login } from "../../api/loginService";

function* loginWorker(action) {
  try {
    const data = {
      username: action.request.username,
      password: action.request.pass,
    };

    const usuario = yield call(login, data);
    if (usuario[0] !== undefined) {
      yield put(Actions.loginSuccess(usuario[0]));
    } else {
      yield put(
        Actions.loginError("El usuario y/o la contraseña no coinciden")
      );
    }
  } catch (e) {
    Actions.loginError(
      "Ha ocurrido un error en el sistema, intente mas tarde."
    );
  }
}

function* loginSaga() {
  yield takeEvery(ActionsTypes.LOGIN_REQUEST, loginWorker);
}

export default loginSaga;
