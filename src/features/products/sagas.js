import { call, put, takeEvery } from "redux-saga/effects";

import { ActionsTypes, Actions } from "./actions";
import { getAll, remove, save, update } from "../../api/taskService";

function* productWorker(action) {
  try {
    const list = yield call(getAll, action.request.id);
    yield put(Actions.productRespose(list));
  } catch (e) {
    console.log(e);
  }
}

function* productDeleteWorker(action) {
  try {
    yield call(remove, action.request.id);
    const list = yield call(getAll, action.request.idUsuario);
    yield put(Actions.productRespose(list));
  } catch (e) {
    console.log(e);
  }
}

function* productAddWorker(action) {
  try {
    const request = {
      idUsuario: action.request.idUsuario,
      descripcion: action.request.descripcion
    };
    yield call(save, request);
    yield put(Actions.setVisible(false));
    yield put(Actions.clearAll());
    const list = yield call(getAll, action.request.idUsuario);
    yield put(Actions.productRespose(list));
  } catch (e) {
    console.log(e);
  }
}

function* productUpdateWorker(action) {
  try {
    const request = {
      id: action.request.id,
      idUsuario: action.request.idUsuario,
      descripcion: action.request.descripcion
    };
    yield call(update, request);
    yield put(Actions.setVisible(false));
    yield put(Actions.setUpdate(false));
    yield put(Actions.clearAll());
    const list = yield call(getAll, action.request.idUsuario);
    yield put(Actions.productRespose(list));
  } catch (e) {
    console.log(e);
  }
}

function* productSaga() {
  yield takeEvery(ActionsTypes.PRODUCT_REQUEST, productWorker);
  yield takeEvery(ActionsTypes.PRODUCT_DELETE, productDeleteWorker);
  yield takeEvery(ActionsTypes.PRODUCT_ADD, productAddWorker);
  yield takeEvery(ActionsTypes.PRODUCT_UPDATE, productUpdateWorker);
}

export default productSaga;
