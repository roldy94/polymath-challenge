import { Modal, Button } from "antd";
import React from "react";

class ModalTask extends React.Component {
  render() {
    const { visible, title, handleOk, handleCancel, btnTextOk,disabled } = this.props;
    return (
      <div>
        <Modal
          closable={false}
          title={title}
          visible={visible}
          footer={[
            <Button onClick={handleCancel}>Cancelar</Button>,
            <Button type="primary" onClick={handleOk} disabled={disabled}>
              {btnTextOk}
            </Button>,
          ]}
        >
          {this.props.children}
        </Modal>
      </div>
    );
  }
}

export default ModalTask;
