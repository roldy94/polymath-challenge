import styled from 'styled-components';
import {
    Badge,
    Col,
    Table,
    Button,
    Layout,
    Row,
    Input
} from 'antd';

export const ContainerTask = styled.div`
    padding: 24px;
    background: #fff;
    min-height: 500px;
    margin: 16px 0;
`;

export const LayoutTask = styled(Layout)`
    padding: 24px;
    background: #fff;
    margin: 16px 0; 
`;
export const InputTask = styled(Input)`
    margin: 5px;
`;

export const TableTask = styled(Table)``;
export const ColTask = styled(Col)``;
export const BadgeTask = styled(Badge)``;
export const ButtonTask = styled(Button)``;
export const RowTask = styled(Row)``;

export const ButtonAddTask = styled(Button)`
    margin-bottom: 10px;
`;

export const InputTaskSearch = styled(Input)`
    margin-bottom: 5px;
`;