
import { ActionsTypes } from '../actions';
import { futimes } from 'fs';

const initialState = {
  products: [],
  productsFilter: [],
  id: "",
  descripcion: "",
  precio: "",
  numTarea: "",
  cantidad: "",
  visible: false,
  update: false,
  searchValue:""
};

export default function productReducer(state = initialState, action = {}) {
  switch (action.type) {
    case ActionsTypes.PRODUCT_REQUEST:
      return {
        ...state,
      };
    case ActionsTypes.PRODUCT_RESPONSE:
      return {
        ...state,
        products: action.response,
        productsFilter: action.response,
      };
    case ActionsTypes.PRODUCT_ADD:
      return {
        ...state,
        descripcion: action.request.descripcion,
        precio: action.request.precio,
        numTarea: action.request.numTarea,
        cantidad: action.request.cantidad,
      };
    case ActionsTypes.PRODUCT_UPDATE:
      return {
        ...state,
        id: action.request.id,
        descripcion: action.request.descripcion,
        precio: action.request.precio,
        numTarea: action.request.numTarea,
        cantidad: action.request.cantidad,
      };
    case ActionsTypes.PRODUCT_DELETE:
      return {
        ...state,
      };
    case ActionsTypes.SET_VISIBLE:
      return {
        ...state,
        visible: action.request,
      };
    case ActionsTypes.SET_DESCRIPCION:
      return {
        ...state,
        descripcion: action.request,
      };
    case ActionsTypes.SET_PRECIO:
      return {
        ...state,
        precio: action.request,
      };
    case ActionsTypes.SET_CATEGORIA:
      return {
        ...state,
        numTarea: action.request,
      };
    case ActionsTypes.SET_CANTIDAD:
      return {
        ...state,
        cantidad: action.request,
      };
    case ActionsTypes.SET_UPDATE:
      return {
        ...state,
        update: action.request,
      };
    case ActionsTypes.SET_ID:
      return {
        ...state,
        id: action.request,
      };
    case ActionsTypes.SET_FILTER:
      return {
        ...state,
        searchValue: action.request,
        productsFilter:action.request ? filterByValue(state.products,action.request) : state.products
      };
    case ActionsTypes.CLEAR_ALL:
      return {
        ...state,
        descripcion: "",
        precio: "",
        numTarea: "",
        cantidad: "",
        id: ""
      };
    default:
      return state;
  }
}


function filterByValue(array, string) {
  return array.filter(o => {
    return Object.keys(o).some(k => {
      if (typeof o[k] === "string")
        return o[k].toLowerCase().includes(string.toLowerCase());
    });
  });
}