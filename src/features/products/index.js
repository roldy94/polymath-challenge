import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Actions } from "./actions";
import {
  RowTask,
  LayoutTask,
  TableTask,
  ColTask,
  BadgeTask,
  ButtonTask,
  ContainerTask,
  InputTask,
  ButtonAddTask,
  InputTaskSearch,
} from "./styles";
import { message } from "antd";
import ModalTask from "./components/modal";

class Task extends React.Component {
  _showModal = () => {
    const { actions } = this.props;
    actions.setVisible(true);
  };

  _showModalUpdate = (request) => {
    const { actions } = this.props;
    actions.setVisible(true);
    actions.setUpdate(true);
    actions.setID(request.id);
    actions.setDescripcion(request.descripcion);
  };

  _handleOk = (e) => {
    const { actions } = this.props;
    const { descripcion, id } = this.props.state;
    actions.productAdd({
      id: id,
      idUsuario: this.props.stateLogin.user.id,
      descripcion: descripcion,
    });
  };

  _handleUpdate = (e) => {
    const { actions } = this.props;
    const { descripcion, id } = this.props.state;
    actions.productUpdate({
      id: id,
      idUsuario: this.props.stateLogin.user.id,
      descripcion: descripcion,
    });
  };

  _handleCancel = (e) => {
    const { actions } = this.props;
    actions.setVisible(false);
    actions.setUpdate(false);
    actions.clearAll();
  };

  componentDidMount() {
    const { actions } = this.props;
    actions.productRequest(this.props.stateLogin.user);
  }
  _handleDelete = (key) => {
    const { actions } = this.props;
    actions.productDelete({
      id: key,
      idUsuario: this.props.stateLogin.user.id,
    });
    message.error("Fila Eliminada");
  };

  render() {
    const columns = [
      {
        title: "#",
        dataIndex: "id",
        key: Math.random(),
        width: 400,
        sorter: (a, b) => a.id.length - b.id.length,
      },
      {
        title: "User",
        dataIndex: "idUsuario",
        key: Math.random(),
        width: 400,
        sorter: (a, b) => a.id.length - b.id.length,
      },
      {
        title: "description",
        dataIndex: "descripcion",
        key: Math.random(),
        width: 400,
      },
      {
        title: "Modificar",
        dataIndex: "",
        key: Math.random(),
        width: 100,
        render: (record) => (
          <ButtonTask
            type="primary"
            shape="circle"
            icon="edit"
            onClick={() => this._showModalUpdate(record)}
          />
        ),
      },
      {
        title: "Eliminar",
        dataIndex: "id",
        key: Math.random(),
        width: 100,
        render: (record) => (
          <ButtonTask
            type="danger"
            shape="circle"
            icon="delete"
            onClick={() => this._handleDelete(record)}
          />
        ),
      },
    ];
    const { actions } = this.props;
    return (
      <ContainerTask>
        <LayoutTask>
          <RowTask style={{ padding: "inherit" }}>
            <ButtonAddTask type="primary" onClick={() => this._showModal()}>
              Agregar
            </ButtonAddTask>

            <ColTask span={24}>
              <InputTaskSearch
                onChange={(e) => actions.setFilter(e.target.value)}
                placeholder="Filtre por descripcion"
                value={this.props.state.searchValue}
              />

              <TableTask
                pagination={{ pageSize: 25 }}
                size="small"
                columns={columns}
                bordered={true}
                title={() => (
                  <span>
                    Numero de tareas:{" "}
                    <BadgeTask count={this.props.state.productsFilter.length} />{" "}
                  </span>
                )}
                locale={{ emptyText: "Sin tareas asignadas" }}
                dataSource={this.props.state.productsFilter}
              />
            </ColTask>
          </RowTask>
        </LayoutTask>
        <ModalTask
          disabled={this.props.state.descripcion === "" ? true : false}
          btnTextOk={this.props.state.update ? "Modificar" : "Agregar"}
          visible={this.props.state.visible}
          title={this.props.state.update ? "Modificar" : "Agregar"}
          handleOk={
            this.props.state.update ? this._handleUpdate : this._handleOk
          }
          handleCancel={this._handleCancel}
        >
          <InputTask
            onChange={(e) => actions.setDescripcion(e.target.value)}
            placeholder="descripcion"
            value={this.props.state.descripcion}
          />
        </ModalTask>
      </ContainerTask>
    );
  }
}

function mapStateToProps(state) {
  return {
    state: state.productReducer,
    stateLogin: state.loginReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...Actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Task);
