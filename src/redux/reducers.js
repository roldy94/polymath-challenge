import  loginReducer  from "../features/login/reducers";
import  homeReducer  from "../features/home/reducers";
import  productReducer  from "../features/products/reducers";

export default {  
    loginReducer,
    homeReducer,
    productReducer
};
  