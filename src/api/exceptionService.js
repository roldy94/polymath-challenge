export async function getAll(){
    return await fetch(
        "http://localhost:8080/exceptions"
    ).then(response =>{
        return response.json();
      }
    )
}

export async function deleteExceptions(id){
    return await fetch(
        "http://localhost:8080/exceptions/"+id,
        {
            method:"DELETE",
            headers:{
                Accept:"application/json",
                "Content-Type":"application/json"
            },
            credentials:'same-origin',
        }
    ).then(response =>{
        return response.json();
    })
}

export async function saveExceptions(request){
    return await fetch(
        "http://localhost:8080/exceptions",
        {
            method:"POST",
            headers:{
                Accept:"application/json",
                "Content-Type":"application/json"
            },
            credentials:'same-origin',
            body:JSON.stringify({
                descripcion:request.descripcion,
                precio:request.precio,
                categoria:request.categoria,
                cantidad:request.cantidad
            })
        }
    ).then(response =>{
        return response.json();
    })
}

export async function updateExceptions(request){
    return await fetch(
        "http://localhost:8080/exceptions/"+request.id,
        {
            method:"PUT",
            headers:{
                Accept:"application/json",
                "Content-Type":"application/json"
            },
            credentials:'same-origin',
            body:JSON.stringify({
                descripcion:request.descripcion,
                precio:request.precio,
                categoria:request.categoria,
                cantidad:request.cantidad
            })
        }
    ).then(response =>{
        return response.json();
    })
}