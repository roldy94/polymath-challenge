export async function getAll(id) {
  return await fetch("http://localhost:8080/tareas?idUsuario=" + id).then(
    (response) => {
      return response.json();
    }
  );
}

export async function remove(id) {
  return await fetch("http://localhost:8080/tareas/" + id, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    credentials: "same-origin",
  }).then((response) => {
    return response.json();
  });
}

export async function save(request) {
  return await fetch("http://localhost:8080/tareas", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    credentials: "same-origin",
    body: JSON.stringify({
      idUsuario: request.idUsuario,
      descripcion: request.descripcion,
    }),
  }).then((response) => {
    return response.json();
  });
}

export async function update(request) {
  return await fetch("http://localhost:8080/tareas/" + request.id, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    credentials: "same-origin",
    body: JSON.stringify({
      idUsuario: request.idUsuario,
      descripcion: request.descripcion,
    }),
  }).then((response) => {
    return response.json();
  });
}
